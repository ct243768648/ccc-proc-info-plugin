# ccc-proc-info-plugin
dde状态栏网速插件



## 编译依赖:

debhelper (>= 11), libdtkwidget-dev,libdtkgui-dev,qtbase5-dev,zlib1g-dev,libgsettings-qt-dev,dde-tray-loader-dev



## 编译安装：

```
mkdir build
cd build

cmake ..
sudo make install
```



## 构建deb安装包：

```
bash ./dpkg-buildpackage.sh
```

