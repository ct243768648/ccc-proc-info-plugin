#include "mainwindow.h"
#include "build_config.h"

#include <DApplication>
#include <DApplicationSettings>
#include <DGuiApplicationHelper>

DWIDGET_USE_NAMESPACE
DCORE_USE_NAMESPACE

int main(int argc, char *argv[])
{
    DApplication a(argc, argv);
    a.setAttribute(Qt::AA_UseHighDpiPixmaps); // 高清图
    a.setOrganizationName("Ccc");
    a.setApplicationName("CccProcCleaner");
    a.setApplicationVersion(BUILD_VERSION);
    a.loadTranslator();
    a.setApplicationDisplayName(QObject::tr("Process Cleaner"));
    a.setStyle("chameleon");
    a.setWindowIcon(QIcon::fromTheme("ProcCleaner"));
    a.setProductIcon(QIcon::fromTheme("ProcCleaner"));
    a.setApplicationDescription(QApplication::translate("main", "清理进程"));
    a.setQuitOnLastWindowClosed(false);

    if (!a.setSingleInstance("CccProcCleaner")) {
        qInfo() << Q_FUNC_INFO << "单例程序，已存在一个进程";
        exit(0);
    }

    DApplicationSettings settings;

    DGuiApplicationHelper::instance()->setPaletteType(DGuiApplicationHelper::ColorType::DarkType);

    MainWindow w;
    w.show();

    QObject::connect(&a, &DApplication::newInstanceStarted, &a, [&w] {
        qInfo() << "newInstanceStarted";
        w.activateWindow();
    });

    a.exec();
}
