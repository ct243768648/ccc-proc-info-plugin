#pragma once

#include "proccleanermodel.h"
#include "simplemodewidget.h"

#include <DBlurEffectWidget>
#include <DIconButton>

#include <QStandardItem>
#include <QItemSelectionModel>

DWIDGET_USE_NAMESPACE
DCORE_USE_NAMESPACE

class MainWindow : public DBlurEffectWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public Q_SLOTS:
    void onNotifyMoveAppViewItem(const QPoint &pos);
    void onNotifyExecAppViewItemDroppedAction(const AppInfo &info, AppViewItemDroppedAction action);

protected:
    void mouseReleaseEvent(QMouseEvent *event);

private:
    void initUI();
    void initData();
    void initConnections();
    void postInit();
    void updateProcInfo();

private:
    ProcCleanerModel *m_model;
    QTimer *m_updateTimer;
    SimpleModeWidget *m_simpleModeWidget;
    QWidget *m_movingAppViewItemWidget;
};
