#include "appviewitem.h"

#include <DStyle>

#include <QMouseEvent>
#include <QTimer>
#include <QVBoxLayout>

DWIDGET_USE_NAMESPACE

AppViewItem::AppViewItem(const AppInfo &info, ProcCleanerModel *model, QWidget *parent)
    : DFrame(parent)
    , m_info(info)
    , m_model(model)
    , m_nameLabel(nullptr)
    , m_lockIconLabel(nullptr)
    , m_memorySizeLabel(nullptr)
    , m_closeBtn(nullptr)
    , m_isIconLabelPressing(false)
    , m_isEnableMove(true)
{
    initUI();
    initData();
    initConnections();
    postInit();
}

AppViewItem::~AppViewItem()
{
}

void AppViewItem::setInfo(const AppInfo &info)
{
    m_info = info;

    QFontMetrics fm(font());
    QString elidedName = fm.elidedText(m_info.name, Qt::TextElideMode::ElideMiddle, m_nameLabel->width());
    m_nameLabel->setText(elidedName);
    m_nameLabel->setToolTip(m_info.name);

    QIcon appIcon;
    if (!m_info.icon.isEmpty()) {
        appIcon = QIcon::fromTheme(m_info.icon);
    }
    if (appIcon.isNull()) {
        appIcon = QIcon::fromTheme(THEME_ICON_NAME_DEFAULT);
    }
     m_iconLabel->setPixmap(appIcon.pixmap(100, 100));

    QIcon lockIcon;
    if (info.isLocked) {
        lockIcon = QIcon::fromTheme(THEME_ICON_NAME_LOCK);
    }
    m_lockIconLabel->setPixmap(lockIcon.pixmap(30, 30));

    // memory size label
    m_memorySizeLabel->setText(formatBytesByDefault(m_info.memorySize));
}

AppInfo AppViewItem::getInfo() const
{
    return m_info;
}

void AppViewItem::enableMove(bool enable)
{
    m_isEnableMove = enable;
}

void AppViewItem::checkWhetherNeedWatchInfo()
{
    if (m_isIconLabelPressing) {
        Q_EMIT requestWatchInfo(m_info.pkgName);
    }
}

bool AppViewItem::eventFilter(QObject *obj, QEvent *e)
{
    bool ret = false;
    while(true) {
        // 点击锁定逻辑
        if (m_iconLabel == obj) {
            if (QEvent::Type::MouseButtonRelease == e->type()) {
                m_isIconLabelPressing = false;
                e->accept();
                ret = true;
                break;
            } else if (QEvent::Type::MouseButtonPress == e->type()) {
                m_isIconLabelPressing = true;
                m_watchInfoCheckTimer->start(1000);
                ret = true;
                break;
            }
        }

        break;
    }

    while(m_isEnableMove) {
        // 点击锁定逻辑
        if (m_iconLabel == obj) {
            if (QEvent::Type::MouseButtonRelease == e->type()) {
                // 当应用显示项被放开时
                m_model->setAppViewItemPressing(false, QPoint());
            } else if (QEvent::Type::MouseButtonPress == e->type()) {
                QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(e);
                // 应用显示项被点击时
                m_originPressedPos = mouseEvent->pos();
                m_model->setAppViewItemPressing(true, mapToGlobal(QPoint()));
                m_model->setPressingAppInfo(m_info);
            } else if (QEvent::Type::MouseMove == e->type()) {
                QMouseEvent *mouseEvent = static_cast<QMouseEvent *>(e);
                if (m_model->isAppViewItemPressing()) {
                   m_model->setPressingAppViewItemPos(mapToGlobal(QPoint()) + mouseEvent->pos() - m_originPressedPos);
                }
            }
        }
        break;
    }

    if (ret) {
        return true;
    }

    return DFrame::eventFilter(obj, e);
}

void AppViewItem::initUI()
{
    setContentsMargins(0, 0, 0, 0);
    setLineWidth(0);

    // 设置背景透明
    QPalette pa = palette();
    pa.setColor(QPalette::Base, Qt::transparent);
    setPalette(pa);

    QHBoxLayout *mainLayout = new QHBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    mainLayout->setAlignment(Qt::AlignCenter);
    setLayout(mainLayout);
//    setStyleSheet("background-color:red");

    // left layout
    QVBoxLayout *leftLayout = new QVBoxLayout;
    leftLayout->setContentsMargins(0, 0, 0, 0);
    leftLayout->setSpacing(0);
    mainLayout->addLayout(leftLayout);

    m_nameLabel = new QLabel(this);
    leftLayout->addWidget(m_nameLabel, 0, Qt::AlignCenter);

    // iconLayout
    leftLayout->addSpacing(3);
    QHBoxLayout *iconLayout = new QHBoxLayout;
    iconLayout->setContentsMargins(0, 0, 0, 0);
    iconLayout->setSpacing(0);
    leftLayout->addLayout(iconLayout);

    m_iconLabel = new QLabel(this);
    m_iconLabel->setMouseTracking(true);
    m_iconLabel->setFixedSize(100, 100);
    iconLayout->addWidget(m_iconLabel, 0, Qt::AlignRight);

    // memory size label
    leftLayout->addSpacing(10);
    m_memorySizeLabel = new QLabel("", this);
    leftLayout->addWidget(m_memorySizeLabel, 0, Qt::AlignCenter);

    // right layout
    QVBoxLayout *rightLayout = new QVBoxLayout;
    rightLayout->setContentsMargins(0, 0, 0, 0);
    rightLayout->setSpacing(0);
    mainLayout->addLayout(rightLayout);

    m_closeBtn = new DIconButton(this);
    m_closeBtn->setFlat(true);
    m_closeBtn->setFixedSize(30, 30);
    m_closeBtn->setIcon(DStyle::StandardPixmap::SP_CloseButton);
    m_closeBtn->setIconSize(QSize(30, 30));
    rightLayout->addWidget(m_closeBtn);

    m_lockIconLabel = new QLabel(this);
    m_lockIconLabel->setFixedSize(30, 30);
    rightLayout->addWidget(m_lockIconLabel);

    mainLayout->addSpacing(5);
}

void AppViewItem::initData()
{
    m_watchInfoCheckTimer = new QTimer(this);
    m_watchInfoCheckTimer->setSingleShot(true);

    setInfo(m_info);
    m_iconLabel->installEventFilter(this);
}

void AppViewItem::initConnections()
{
    connect(m_watchInfoCheckTimer, &QTimer::timeout, this, &AppViewItem::checkWhetherNeedWatchInfo);
    connect(m_closeBtn, &DIconButton::clicked, this, [this] {
        Q_EMIT this->requestCloseApp(m_info);
    });
}

void AppViewItem::postInit()
{
}
