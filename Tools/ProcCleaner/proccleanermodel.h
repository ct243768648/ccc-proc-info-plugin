#pragma once

#include "../../src/common/common.h"
#include "../../src/common/procinfolist.h"
#include "../../src/common/utils/utils.h"

#include <DWindowManagerHelper>
#include <DForeignWindow>

#include <QObject>
#include <QDBusInterface>

#define MAX_PROPERTY_VALUE_LEN 4096    //取得属性值最大长度

// 数据处理类
class ProcCleanerModel : public QObject
{
    Q_OBJECT
private:
    void initData();
    bool loadCfgs();
    void saveCfgs();

    void asyncKillProc(const QList<uint> &pids);
    uint getPidByWid(ulong wid);
    QWindow *getXWindowByPid(uint pid);
    int killXWindow(const QWindow *w);

public:
    struct MemInfo {
        ulong TotalMem; // 总内存，单位：byte
        ulong AvailableMem; // 可用内存，单位：byte
        MemInfo() {
            TotalMem = 0;
            AvailableMem = 0;
        }
    };

    explicit ProcCleanerModel(QObject *parent = nullptr);
    ~ProcCleanerModel();

    ProcInfoList getProcInfoList() const;

    MemInfo getMemInfo() const; // 获取内存信息
    ulong getProcMem(uint pid) const;

    void setAppLockState(const QString &pkgName, bool lock);
    bool getAppLockState(const QString &pkgName) const;

    void asyncKillApps(const QList<AppInfo> &apps);

    void setAppViewItemPressing(bool pressing, const QPoint &originPos);
    bool isAppViewItemPressing();
    void setPressingAppViewItemPos(const QPoint &pos);
    void setPressingAppInfo(const AppInfo &info);
    AppInfo getPressingAppInfo();

public Q_SLOTS:
    void onKillProcFinished(const QList<uint> &pids);

Q_SIGNALS:
    void killProcFinished(const QList<uint> &pids);
    // 通知移动应用显示项
    void notifyMoveAppViewItem(const QPoint &pos);
    //  通知去设置应用项是否可见
    void notifySetUiAppViewItemVisible(const AppInfo &info, bool visible);
    // 通知执行应用显示项放下动作
    void notifyExecAppViewItemDroppedAction(const AppInfo &info, AppViewItemDroppedAction action);

private:
    QDBusInterface *m_netMonitorInter;
    QStringList m_memCleanPkgNameWhitelist;
    //// 应用显示项移动相关
    bool m_isPressingAppViewItem = false;
    QPoint m_pressingAppViewItemOriPos;
    QPoint m_pressingAppViewItemPos;
    bool m_isMovingAppViewItem = false;
    AppInfo m_pressingAppInfo;
    //// - end - 应用显示项移动相关
};
