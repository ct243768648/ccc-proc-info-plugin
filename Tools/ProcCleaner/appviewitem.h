#pragma once

#include "../../src/common/utils/utils.h"
#include "../../src/common/common.h"
#include "proccleanermodel.h"

#include <DFrame>
#include <DTipLabel>
#include <DIconButton>

#include <QLabel>
#include <QPushButton>

DWIDGET_USE_NAMESPACE

#define THEME_ICON_NAME_LOCK "lock"

class AppViewItem : public DFrame
{
    Q_OBJECT
public:
    explicit AppViewItem(const AppInfo &info = AppInfo(), ProcCleanerModel *model = nullptr, QWidget *parent = nullptr);
    virtual ~AppViewItem() override;

    void setInfo(const AppInfo &info);
    AppInfo getInfo() const;

    void enableMove(bool enable);

public Q_SLOTS:
    void checkWhetherNeedWatchInfo();

Q_SIGNALS:
    void requestWatchInfo(const QString &pkgName);
    void requestCloseApp(const AppInfo &info);

protected:
    virtual bool eventFilter(QObject *obj, QEvent *e) override;

private:
    void initUI();
    void initData();
    void initConnections();
    void postInit();

private:
    AppInfo m_info;
    ProcCleanerModel *m_model;
    QLabel *m_iconLabel;
    QLabel *m_nameLabel;
    QLabel *m_lockIconLabel;
    QLabel *m_memorySizeLabel;
    DIconButton *m_closeBtn;
    QTimer *m_watchInfoCheckTimer;
    bool m_isIconLabelPressing = false;
    QPoint m_originPressedPos;

    bool m_isEnableMove;
};
