#pragma once

#include "appviewitem.h"

#include <DListView>

DWIDGET_USE_NAMESPACE

class AppListView : public QListView
{
    Q_OBJECT
public:
    explicit AppListView(QWidget *parent = nullptr);
    ~AppListView();

    void appendAppViewItem(AppViewItem *item);

public Q_SLOTS:

protected:
    void mouseReleaseEvent(QMouseEvent *event);

private:
    void initUI();
    void initData();
    void initConnections();
    void postInit();

private:
};
