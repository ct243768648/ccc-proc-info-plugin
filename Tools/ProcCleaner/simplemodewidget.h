#pragma once

#include "proccleanermodel.h"
#include "applistview.h"

#include <DFrame>
#include <DListView>
#include <QMouseEvent>

#define VIEW_ITEM_SIZE QSize(150, 150)

DWIDGET_USE_NAMESPACE
DCORE_USE_NAMESPACE

class SimpleModeWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SimpleModeWidget(ProcCleanerModel *model, QWidget *parent = nullptr);
    ~SimpleModeWidget();

    void updateProcInfo();
public Q_SLOTS:
    void onItemRequestSetAppLockState(const QString &pkgName, bool lock);
    void onItemRequestCloseApp(const AppInfo &info);
    // 当被通知去设置应用项是否可见
    void onNotifySetAppViewItemVisible(const AppInfo &info, bool visible);

    void cleanApps();

Q_SIGNALS:

protected:

private:
    void initUI();
    void initData();
    void initConnections();
    void postInit();

    void appendAppViewItem(const AppInfo &info);
    void removeAppViewItem(const QString &pkgName);
    void updateAppViewItem(const AppInfo &info);

private:
    ProcCleanerModel *m_model;
    QStandardItemModel *m_appListViewModel;
    AppListView *m_appListView;
    QLabel *m_totalMemoryUsageLabel;
    QPushButton *m_cleanBtn;
    QMap<QString, AppInfo> m_pkgNameToAppInfoMap;
    QList<QString> m_pkgNameListOfkillingApp; // 正在后台关闭中的app对应的包名列表
};
