#include "mainwindow.h"
#include "../../src/common/utils/utils.h"

#include <DTitlebar>
#include <DDialog>

#include <QVBoxLayout>
#include <QTextEdit>
#include <QApplication>
#include <QPushButton>
#include <QScreen>

MainWindow::MainWindow(QWidget *parent)
    : DBlurEffectWidget(parent)
    , m_model(new ProcCleanerModel(this))
    , m_updateTimer(nullptr)
    , m_simpleModeWidget(nullptr)
    , m_movingAppViewItemWidget(nullptr)
{
    initUI();
    initData();
    initConnections();
    postInit();
}

MainWindow::~MainWindow()
{
}

void MainWindow::onNotifyMoveAppViewItem(const QPoint &pos)
{
    if (!m_movingAppViewItemWidget) {
        m_movingAppViewItemWidget = new QWidget(this);
        m_movingAppViewItemWidget->setFixedSize(VIEW_ITEM_SIZE);

        QVBoxLayout *itemWidgetMainLayout = new QVBoxLayout;
        itemWidgetMainLayout->setContentsMargins(0, 0, 0, 0);
        m_movingAppViewItemWidget->setLayout(itemWidgetMainLayout);

        AppViewItem *movingAppViewItemTmp = new AppViewItem(m_model->getPressingAppInfo(), m_model, m_movingAppViewItemWidget);
        movingAppViewItemTmp->enableMove(false);
        itemWidgetMainLayout->addWidget(movingAppViewItemTmp, 0, Qt::AlignLeft);

        m_movingAppViewItemWidget->show();
        m_movingAppViewItemWidget->raise();
    }

    m_movingAppViewItemWidget->move(mapFromGlobal(pos));
}

void MainWindow::onNotifyExecAppViewItemDroppedAction(const AppInfo &info, AppViewItemDroppedAction action)
{
    switch (action) {
    case Restore:
        m_simpleModeWidget->onNotifySetAppViewItemVisible(info, true);
        break;
    case Remove:
        m_simpleModeWidget->onItemRequestCloseApp(info);
        break;
    case Lock: {
        bool isLocked = m_model->getAppLockState(info.pkgName);
        m_simpleModeWidget->onItemRequestSetAppLockState(info.pkgName, !isLocked);
        m_simpleModeWidget->onNotifySetAppViewItemVisible(info, true);
        break;
    }
    default:
        break;
    }

    if (m_movingAppViewItemWidget) {
        m_movingAppViewItemWidget->deleteLater();
        m_movingAppViewItemWidget = nullptr;
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    DBlurEffectWidget::mouseReleaseEvent(event);

    if (Qt::MouseButton::LeftButton == event->button()) {
        qApp->exit();
    }
}

void MainWindow::initUI()
{
    QRect primaryScreenGeometry = qApp->primaryScreen()->geometry();
    setGeometry(primaryScreenGeometry);

    setWindowFlags(Qt::WindowType::WindowStaysOnTopHint
                   | Qt::WindowType::MaximizeUsingFullscreenGeometryHint);
    setWindowState(Qt::WindowState::WindowFullScreen);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    mainLayout->addSpacing(50);
    QWidget *contentWidget = new QWidget(this);
    mainLayout->addWidget(contentWidget, 1);

    QVBoxLayout *contentLayout = new QVBoxLayout;
    contentLayout->setContentsMargins(0, 0, 0, 0);
    contentLayout->setSpacing(0);
    contentWidget->setLayout(contentLayout);

    // 添加内容
    m_simpleModeWidget = new SimpleModeWidget(m_model, this);
    contentLayout->addWidget(m_simpleModeWidget);
}

void MainWindow::initData()
{
    m_updateTimer = new QTimer(this);
    m_updateTimer->setInterval(2 * 1000);
}

void MainWindow::initConnections()
{
    //// connection
    // 更新定时器
    connect(m_updateTimer, &QTimer::timeout, this, &MainWindow::updateProcInfo);
    // 通知移动应用显示项
    connect(m_model, &ProcCleanerModel::notifyMoveAppViewItem, this, &MainWindow::onNotifyMoveAppViewItem);
    // 通知执行应用显示项放下动作
    connect(m_model, &ProcCleanerModel::notifyExecAppViewItemDroppedAction, this, &MainWindow::onNotifyExecAppViewItemDroppedAction);
}

void MainWindow::postInit()
{
    updateProcInfo();
    m_updateTimer->start();
}

void MainWindow::updateProcInfo()
{
    m_simpleModeWidget->updateProcInfo();
}
