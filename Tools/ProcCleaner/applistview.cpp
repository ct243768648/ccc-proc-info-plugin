#include "applistview.h"

#include <QMouseEvent>
#include <QVBoxLayout>

#include <QApplication>

AppListView::AppListView(QWidget *parent)
    : QListView(parent)
{
    initUI();
    initData();
    initConnections();
    postInit();
}

AppListView::~AppListView()
{
}

void AppListView::mouseReleaseEvent(QMouseEvent *event)
{
    QListView::mouseReleaseEvent(event);

    if (Qt::MouseButton::LeftButton == event->button()) {
        qApp->exit();
    }
}

void AppListView::initUI()
{
    // 设置背景透明
    QPalette pa = palette();
    pa.setColor(QPalette::Base, Qt::transparent);
    setPalette(pa);

    setLineWidth(0);
}

void AppListView::initData()
{
}

void AppListView::initConnections()
{
}

void AppListView::postInit()
{
}
