#include "procinfosrvmodel.h"

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <stdio.h>

void ProcInfoSrvModel::initData()
{
    if (!QDBusConnection::sessionBus().registerService(service) || !QDBusConnection::sessionBus().registerObject(path, this)) {
        qCritical() << Q_FUNC_INFO << service << "is invalid!";
        exit(0);
    }
}

ProcInfoSrvModel::ProcInfoSrvModel(QObject *parent)
    : QObject(parent)
    , m_adaptor(new ProcInfoAdaptor(this))
{
    initData();
}

ProcInfoSrvModel::~ProcInfoSrvModel()
{
}

int ProcInfoSrvModel::KillXClient(qulonglong wid)
{
    qInfo() << Q_FUNC_INFO << wid;
    Display *disp = XOpenDisplay(nullptr);
    int ret = XKillClient(disp, wid);
    XCloseDisplay(disp);

    return ret;
}

void ProcInfoSrvModel::AsyncExecProgram(const QString &program, const QStringList &arguments)
{
    QProcess proc;
    proc.startDetached(program, arguments);
}
