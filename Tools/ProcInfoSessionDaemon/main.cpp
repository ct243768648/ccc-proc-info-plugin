
#include "procinfosrvmodel.h"

#include <DApplication>

#include <QFile>

// 构建版本配置文件路径
#define CMAKE_PKG_CONF_FILE_PATH_MAJOR_VERSION ":/cmakepkgconf/majorver.txt"
#define CMAKE_PKG_CONF_FILE_PATH_MINOR_VERSION ":/cmakepkgconf/minorver.txt"
#define CMAKE_PKG_CONF_FILE_PATH_PATCH_VERSION ":/cmakepkgconf/patchver.txt"

DWIDGET_USE_NAMESPACE
DCORE_USE_NAMESPACE

int main(int argc, char *argv[])
{
    DApplication a(argc, argv);
    a.setAttribute(Qt::AA_UseHighDpiPixmaps); // 高清图
    a.setOrganizationName("ccc");
    a.setApplicationName("ccc-proc-info-session-daemon");
    // 从构建脚本中获取版本
    QString  majorVersion;
    QString  minorVersion;
    QString  patchVersion;
    QFile f(CMAKE_PKG_CONF_FILE_PATH_MAJOR_VERSION);
    if (f.open(QIODevice::OpenModeFlag::ReadOnly)) {
        majorVersion = f.readLine().trimmed();
        f.close();
    }
    f.setFileName(CMAKE_PKG_CONF_FILE_PATH_MINOR_VERSION);
    if (f.open(QIODevice::OpenModeFlag::ReadOnly)) {
        minorVersion = f.readLine().trimmed();
        f.close();
    }
    f.setFileName(CMAKE_PKG_CONF_FILE_PATH_PATCH_VERSION);
    if (f.open(QIODevice::OpenModeFlag::ReadOnly)) {
        patchVersion = f.readLine().trimmed();
        f.close();
    }
    QString version = QString("%1.%2.%3").arg(majorVersion).arg(minorVersion).arg(patchVersion);
    if (majorVersion.isEmpty() || minorVersion.isEmpty() || patchVersion.isEmpty()) {
        qInfo() << Q_FUNC_INFO << "not specify version, or specified version format is error, use default version!";
        version = "0.0.1";
    }
    a.setApplicationVersion(version);
    a.loadTranslator();
    a.setApplicationDisplayName(QObject::tr("Process Information Session Daemon"));
    a.setStyle("chameleon");
    a.setApplicationDescription(QApplication::translate("main", "进程信息用户服务"));
    a.setQuitOnLastWindowClosed(false);

    ProcInfoSrvModel model;
    a.exec();
}
