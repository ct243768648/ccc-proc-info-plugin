#pragma once

#include "procinfoadaptor.h"

#include <QObject>
#include <QDBusInterface>

const QString service = "com.ccc.daemon.ProcInfo";
const QString path = "/com/ccc/daemon/ProcInfo";

// 数据处理类
class ProcInfoSrvModel : public QObject
{
    Q_OBJECT
private:
    void initData();

public:
    explicit ProcInfoSrvModel(QObject *parent = nullptr);
    ~ProcInfoSrvModel();


public Q_SLOTS:
    int KillXClient(qulonglong wid);
    void AsyncExecProgram(const QString &program, const QStringList &arguments);

Q_SIGNALS:

private:
    ProcInfoAdaptor *m_adaptor;
};
