#pragma once

#include "widgets/ctableview/ctableview.h"
#include "procinfomodel.h"
#include "widgets/settingswidget.h"

#include <DBlurEffectWidget>

#include <QStandardItem>
#include <QItemSelectionModel>

DWIDGET_USE_NAMESPACE
DCORE_USE_NAMESPACE

class MainWindow : public DBlurEffectWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    // 打开设置界面
    void openSettingsPage();

public Q_SLOTS:
    void onContextMenuTriggered(QAction *action);

protected:
    void closeEvent(QCloseEvent *e);

private:
    void initUI();
    void initData();
    void initConnections();
    void postInit();
    void updateProcInfo();
    ProcInfo getInfoFromModel(int row);
    void setInfoForItems(const ProcInfo &info, QStandardItem *pidItem,
                         QStandardItem *nameItem, QStandardItem *uploadSpeedItem,
                         QStandardItem *downloadSpeedItem, QStandardItem *totalSpeedItem);
    void hideUnknownItem();

private:
    ProcInfoModel *m_model;
    QAction *m_settingsAction;
    QAction *m_createProcCleanerDesktopAction;
    SettingsWidget *m_settingsPage;
    QTimer *m_updateTimer;
    QStandardItemModel *m_listViewItemModel;
    CTableView *m_tableView;
    QItemSelectionModel *m_listViewSelectionModel;
    int m_sortingIndex;
    Qt::SortOrder m_sortingOrder;
    // 右键菜单
    QMenu *m_contextMenu;
    QAction *m_detailAction;
};
