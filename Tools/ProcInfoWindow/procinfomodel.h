#pragma once

#include "../../src/common/common.h"
#include "../../src/common/procinfolist.h"

#include <DDialog>

#include <QObject>
#include <QDBusInterface>

DWIDGET_USE_NAMESPACE

// 数据处理类
class ProcInfoModel : public QObject
{
    Q_OBJECT
public:
    explicit ProcInfoModel(QObject *parent = nullptr);
    ~ProcInfoModel();

private:
    void initData();
    bool loadCfgsFromFilePath(const QString &path);
    void loadCfgs();
    void saveCfgs();

    bool isPkgInstalled(const QString &pkgName);
    void checkAndNotifyInstallCccSysDaemon();

public:
    bool isEnableNetDataMonitor();
    void enableNetDataMonitor(bool enable);
    bool isShowNetUnKnownItem();
    void showNetUnKnownItem(bool show);
    ProcInfoList getProcInfoList();

    void createProcCleanerDesktop();

Q_SIGNALS:
    void netDataMonitorSwitchChanged(bool enable);
    void showNetUnKnownItemSwitchChanged(bool show);

private:
    QDBusInterface *m_netMonitorInter;
    bool m_enableNetDataMonitor;
    bool m_showNetUnKnownItem;
};
