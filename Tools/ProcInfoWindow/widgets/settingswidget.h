#pragma once

#include "../procinfomodel.h"

#include <DBlurEffectWidget>
#include <DFrame>

DWIDGET_USE_NAMESPACE

class SettingsWidget : public DBlurEffectWidget
{
    Q_OBJECT
public:
    explicit SettingsWidget(ProcInfoModel *model, DBlurEffectWidget *parent = nullptr);
    ~SettingsWidget();

Q_SIGNALS:
    void closed();

private:
    DFrame *createNormalItemFrame(QWidget *leftWidget, QWidget *middleWidget = nullptr, QWidget *rightWidget = nullptr);

private:
    ProcInfoModel *m_model;
};
