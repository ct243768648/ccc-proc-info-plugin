#include "settingswidget.h"

#include <DIconButton>
#include <DApplicationHelper>
#include <DFrame>
#include <DSwitchButton>
#include <DFontSizeManager>
#include <DWidgetUtil>

#include <QDebug>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QLabel>
#include <QCheckBox>

DCORE_USE_NAMESPACE

SettingsWidget::SettingsWidget(ProcInfoModel *model, DBlurEffectWidget *parent)
    : DBlurEffectWidget(parent)
    , m_model(model)
{
    setWindowModality(Qt::WindowModality::ApplicationModal);
    setFocusPolicy(Qt::FocusPolicy::StrongFocus);
    resize(700, 500);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    DIconButton *closeBtn = new DIconButton(this);
    closeBtn->setFixedSize(30, 30);
    closeBtn->setIcon(DStyle::StandardPixmap::SP_CloseButton);
    closeBtn->setIconSize(QSize(30, 30));
    closeBtn->setFlat(true);
    mainLayout->addWidget(closeBtn, 0, Qt::AlignmentFlag::AlignRight | Qt::AlignTop);

    // content
    QHBoxLayout *contentLayout = new QHBoxLayout;
    contentLayout->setContentsMargins(5, 0, 5, 5);
    contentLayout->setSpacing(0);
    mainLayout->addLayout(contentLayout);

    QTreeWidget *overViewTreeWidget = new QTreeWidget(this);
    overViewTreeWidget->setHeaderHidden(true);
    DPalette pa = DApplicationHelper::instance()->palette(overViewTreeWidget);
    pa.setColor(DPalette::ColorRole::Base, Qt::transparent);
    DApplicationHelper::instance()->setPalette(overViewTreeWidget, pa);
    contentLayout->addWidget(overViewTreeWidget, 3);

    QTreeWidgetItem *basicSettingsItem = new QTreeWidgetItem(overViewTreeWidget);
    basicSettingsItem->setText(0, "基础设置");
    overViewTreeWidget->addTopLevelItem(basicSettingsItem);

    QTreeWidgetItem *netTopItem = new QTreeWidgetItem(overViewTreeWidget);
    netTopItem->setText(0, "网络");

    QTreeWidgetItem *netMonitorItem = new QTreeWidgetItem(netTopItem);
    netMonitorItem->setText(0, "流量监控");
    netTopItem->addChild(netMonitorItem);
    overViewTreeWidget->addTopLevelItem(netTopItem);

    // settings Content Layout
    contentLayout->addSpacing(5);
    QVBoxLayout *settingsContentLayout = new QVBoxLayout;
    settingsContentLayout->setContentsMargins(0, 0, 0, 0);
    settingsContentLayout->setSpacing(0);
    settingsContentLayout->setAlignment(Qt::AlignmentFlag::AlignTop);
    contentLayout->addLayout(settingsContentLayout, 7);

    // 基础设置
    QLabel *basicSettingsLable = new QLabel(this);
    basicSettingsLable->setText("基础设置");
    DFontSizeManager::instance()->bind(basicSettingsLable, DFontSizeManager::SizeType::T4);
    settingsContentLayout->addWidget(basicSettingsLable);
    // 空
    settingsContentLayout->addSpacing(10);
    QLabel *blankLable = new QLabel(this);
    blankLable->setText("（空）");
    DFontSizeManager::instance()->bind(blankLable, DFontSizeManager::SizeType::T5);
    settingsContentLayout->addWidget(blankLable);

    // 网络设置
    settingsContentLayout->addSpacing(10);
    QLabel *netSettingsLable = new QLabel(this);
    netSettingsLable->setText("网络设置");
    DFontSizeManager::instance()->bind(netSettingsLable, DFontSizeManager::SizeType::T4);
    settingsContentLayout->addWidget(netSettingsLable);

    settingsContentLayout->addSpacing(10);
    QLabel *netMonitorLable = new QLabel(this);
    netMonitorLable->setText("流量监控");
    DFontSizeManager::instance()->bind(netMonitorLable, DFontSizeManager::SizeType::T5);
    settingsContentLayout->addWidget(netMonitorLable);

    // net monitor switch
    settingsContentLayout->addSpacing(5);
    QLabel *netDataMonitorSwitchTitleLabel = new QLabel(this);
    netDataMonitorSwitchTitleLabel->setText("开启流量监控");

    DSwitchButton *netDataMonitorSwitchBtn = new DSwitchButton(this);
    netDataMonitorSwitchBtn->setChecked(m_model->isEnableNetDataMonitor());

    DFrame *netDataMonitorSwitchFrame = createNormalItemFrame(netDataMonitorSwitchTitleLabel, nullptr, netDataMonitorSwitchBtn);
    settingsContentLayout->addWidget(netDataMonitorSwitchFrame, 0, Qt::AlignTop);

    // 是否显示网络数据未识别的进程项
    settingsContentLayout->addSpacing(5);
    QLabel *showNetUnknownItemTitleLabel = new QLabel(this);
    showNetUnknownItemTitleLabel->setText("显示未识别出网络数据的进程项");

    DSwitchButton *showNetUnknownItemSwitchBtn = new DSwitchButton(this);
    showNetUnknownItemSwitchBtn->setChecked(m_model->isShowNetUnKnownItem());

    DFrame *showNetUnknownItemSwitchFrame = createNormalItemFrame(showNetUnknownItemTitleLabel, nullptr, showNetUnknownItemSwitchBtn);
    settingsContentLayout->addWidget(showNetUnknownItemSwitchFrame, 0, Qt::AlignTop);

    //// connection
    connect(closeBtn, &DIconButton::clicked, this, [this]{
        this->hide();
        Q_EMIT closed();
    });

    connect(this, &SettingsWidget::destroyed, this, [] {
        qInfo() << "SettingsWidget destroyed";
    });

    connect(netDataMonitorSwitchBtn, &DSwitchButton::clicked, this, [this]() {
        bool enable = !m_model->isEnableNetDataMonitor();
        m_model->enableNetDataMonitor(enable);
    });
    connect(m_model, &ProcInfoModel::netDataMonitorSwitchChanged, this, [this, netDataMonitorSwitchBtn] {
        netDataMonitorSwitchBtn->setChecked(m_model->isEnableNetDataMonitor());
    });

    connect(showNetUnknownItemSwitchBtn, &DSwitchButton::clicked, this, [this]() {
        bool show = !m_model->isShowNetUnKnownItem();
        m_model->showNetUnKnownItem(show);
    });
    connect(m_model, &ProcInfoModel::showNetUnKnownItemSwitchChanged, this, [this, showNetUnknownItemSwitchBtn] {
        showNetUnknownItemSwitchBtn->setChecked(m_model->isShowNetUnKnownItem());
    });

    // post init
    overViewTreeWidget->expandItem(netTopItem);
    overViewTreeWidget->setCurrentItem(netMonitorItem);

    Dtk::Widget::moveToCenter(this);
}

SettingsWidget::~SettingsWidget()
{
}

DFrame *SettingsWidget::createNormalItemFrame(QWidget *leftWidget, QWidget *middleWidget, QWidget *rightWidget)
{
    QHBoxLayout *itemFrameLayout = new QHBoxLayout;
    itemFrameLayout->setContentsMargins(0, 0, 0, 0);
    itemFrameLayout->setSpacing(0);

    DFrame *itemFrame = new DFrame(this);
    itemFrame->setBackgroundRole(DPalette::ItemBackground);
    itemFrame->setLayout(itemFrameLayout);

    itemFrameLayout->addSpacing(10);
    itemFrameLayout->addWidget(leftWidget, 0, Qt::AlignLeft);

    if (middleWidget) {
        itemFrameLayout->addSpacing(5);
        itemFrameLayout->addWidget(middleWidget, 0, Qt::AlignLeft);
    }

    itemFrameLayout->addWidget(rightWidget, 0, Qt::AlignRight);
    itemFrameLayout->addSpacing(10);

    return itemFrame;
}
