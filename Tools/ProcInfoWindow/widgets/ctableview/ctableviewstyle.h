#pragma once

#include <QCommonStyle>

class CTableViewStyle : public QCommonStyle
{
    Q_OBJECT

public:
    explicit CTableViewStyle();
    virtual ~CTableViewStyle() override;

    virtual int styleHint(StyleHint stylehint, const QStyleOption *opt = nullptr,
                          const QWidget *widget = nullptr, QStyleHintReturn* returnData = nullptr) const override;
protected:

private:
};
