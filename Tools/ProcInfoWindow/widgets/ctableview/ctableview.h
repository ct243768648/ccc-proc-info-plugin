#pragma once

#include "ctableviewdelegate.h"
#include "ctableheaderview.h"

#include <QTableView>
#include <QHeaderView>
#include <QMouseEvent>

class CTableView : public QTableView
{
    Q_OBJECT

public:
    explicit CTableView(QWidget *parent = nullptr);
    virtual ~CTableView() override;

    void setHeaderBgTransparent(bool transparent);

protected:
    virtual bool eventFilter(QObject *obj, QEvent *event) override;

private:
    void updateCurrentHoveringIndex();

private:
    CTableViewDelegate *m_delegate;
    CTableHeaderView *m_horizontalHeaderView;
    QModelIndex m_currentHoveringIndex;
};
