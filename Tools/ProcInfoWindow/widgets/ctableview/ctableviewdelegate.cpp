#include "ctableviewdelegate.h"

const int Radius = 9;

CTableViewDelegate::CTableViewDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}

CTableViewDelegate::~CTableViewDelegate()
{
}

void CTableViewDelegate::setCurrentHoveringIndex(const QModelIndex &index)
{
    m_currentHoveringIndex = index;
}

void CTableViewDelegate::paint(QPainter *painter, const QStyleOptionViewItem &item, const QModelIndex &index) const
{
    DPalette palette = DApplicationHelper::instance()->palette(item.widget);
    if (index.row() % 2 == 1) {
        QBrush brush = palette.brush(DPalette::ColorType::ItemBackground);
        drawUnitBackground(painter, item, index, brush);
    }

    // 画当鼠标悬浮此处时的背景
    bool isHovering = (index.row() == m_currentHoveringIndex.row());
    if (isHovering) {
        QBrush brush = palette.brush(DPalette::ColorType::ObviousBackground);
        drawUnitBackground(painter, item, index, brush);
    }

    // 画当被选中时的背景
    bool isSelection = (QStyle::StateFlag::State_Selected & item.state);
    if (isSelection) {
        QBrush brush = palette.brush(DPalette::ColorRole::Highlight);
        drawUnitBackground(painter, item, index, brush);
    }

    // 忽略原始选中状态
    QStyleOptionViewItem modifiedOptionItem = item;
    modifiedOptionItem.state = item.state
            & (~QStyle::StateFlag::State_Selected) // 不显示某行某列选中背景
            & (~QStyle::StateFlag::State_HasFocus) // 不显示某行某列聚焦框
            | QStyle::StateFlag::State_Active; // 始终加上激活状态
    QStyledItemDelegate::paint(painter, modifiedOptionItem, index);
}

bool CTableViewDelegate::editorEvent(QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    return QStyledItemDelegate::editorEvent(event, model, option, index);
}

void CTableViewDelegate::drawUnitBackground(QPainter *painter, const QStyleOptionViewItem &item,
                                            const QModelIndex &index, const QBrush &brush) const
{
    QPainterPath path;
    path.setFillRule(Qt::WindingFill);
    if (index.column() == 0) {
        QPainterPath leftPath;
        leftPath.addRoundedRect(item.rect, Radius, Radius);
        QPainterPath rightPath;
        QRect temp_rect(item.rect.left() + Radius, item.rect.top(), item.rect.width() - Radius, item.rect.height());
        rightPath.addRect(temp_rect);

        path = leftPath.united(rightPath);
    } else if (index.column() == index.model()->columnCount() - 1) {
        QPainterPath leftPath;
        leftPath.addRoundedRect(item.rect, Radius, Radius);
        QPainterPath rightPath;
        QRect temp_rect(item.rect.left(), item.rect.top(), item.rect.width() - Radius, item.rect.height());
        rightPath.addRect(temp_rect);

        path = leftPath.united(rightPath);
    } else {
        QRect tmpRect(item.rect.left(), item.rect.top() + 1, item.rect.width(), item.rect.height() - 2);
        path.addRect(item.rect);
    }

    painter->save();
    painter->setOpacity(1);
    painter->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

    painter->fillPath(path, brush);
    painter->restore();
}
