#pragma once

#include <DApplicationHelper>

#include <QPainter>
#include <QPainterPath>
#include <QStyledItemDelegate>

DWIDGET_USE_NAMESPACE

class CTableViewDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit CTableViewDelegate(QObject *parent = nullptr);
    virtual ~CTableViewDelegate() override;

    void setCurrentHoveringIndex(const QModelIndex &index);

protected:
    void paint(QPainter *painter,
               const QStyleOptionViewItem &option, const QModelIndex &index) const override;

    virtual bool editorEvent(QEvent *event,
                             QAbstractItemModel *model,
                             const QStyleOptionViewItem &option,
                             const QModelIndex &index) override;

private:
    void drawUnitBackground(QPainter *painter,const QStyleOptionViewItem &item,
                            const QModelIndex &index, const QBrush &brush) const;

private:
    QModelIndex m_currentHoveringIndex;
};
