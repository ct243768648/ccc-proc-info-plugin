#include "ctableviewstyle.h"

CTableViewStyle::CTableViewStyle()
{

}

CTableViewStyle::~CTableViewStyle()
{
}

int CTableViewStyle::styleHint(QStyle::StyleHint stylehint, const QStyleOption *opt, const QWidget *widget, QStyleHintReturn *returnData) const
{
    switch(stylehint)  
    {
    case SH_ItemView_ShowDecorationSelected:
        return int(false);
    default:
        return QCommonStyle::styleHint(stylehint, opt, widget, returnData);
    }
}
