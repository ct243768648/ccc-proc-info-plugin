#pragma once

#include "procinfo.h"

typedef QList<ProcInfo> ProcInfoList;
Q_DECLARE_METATYPE(ProcInfoList);

void registerProcInfoListMetaType();
