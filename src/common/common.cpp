
#include "common.h"

// 注册会话信息dbus结构体重载操作符函数
QDBusArgument &operator<<(QDBusArgument &argument, const LoginSessionInfo &info)
{
    argument.beginStructure();
    argument << info.id << info.uid << info.name << info.seat << info.objPath;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, LoginSessionInfo &info)
{
    argument.beginStructure();
    argument >> info.id >> info.uid >> info.name >> info.seat >> info.objPath;
    argument.endStructure();
    return argument;
}
