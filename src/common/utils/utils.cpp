#include "utils.h"

#include <QLocale>
#include <QSettings>
#include <QTextCodec>
#include <QDebug>

#define X_DEEPIN_VENDOR_STR "deepin"

QString formatBytes(double input, int prec)
{
    QString flowValueStr;
    if (MBCount > input) {
        flowValueStr = QString::number(input / KBCount, 'd', prec) + "KB";
    } else if (GBCount > input) {
        flowValueStr = QString::number(input / MBCount, 'd', prec) + "MB";
    } else if (TBCount > input) {
        flowValueStr = QString::number(input / GBCount, 'd', prec) + "GB";
    } else if (TBCount >= input) {
        // 大于TB单位
        flowValueStr = QString::number(input / TBCount, 'd', prec) + "TB";
    }
    return flowValueStr;
}

QString formatBytesByDefault(unsigned long input)
{
    const double bytesDouble = input;
    QString flowValueStr = "0.0K";
    if (DisplayMBCount > bytesDouble) {
        double kbValue = bytesDouble / KBCount;
        if (0.01 > kbValue) {
            flowValueStr = "0.0K";
        } else if (10 > kbValue) {
            flowValueStr = QString::number(kbValue, 'f', 2) + "K";
        } else if (100 > kbValue) {
            flowValueStr = QString::number(kbValue, 'f', 1) + "K";
        } else if (100 <= kbValue) {
            flowValueStr = QString::number(kbValue, 'f', 0) + "K";
        }
    } else if (DisplayGBCount > bytesDouble) {
        double dMBValue = bytesDouble / MBCount;
        if (10 > dMBValue) {
            flowValueStr = QString::number(dMBValue, 'f', 2) + "M";
        } else if (100 > dMBValue) {
            flowValueStr = QString::number(dMBValue, 'f', 1) + "M";
        } else if (100 <= dMBValue) {
            flowValueStr = QString::number(dMBValue, 'f', 0) + "M";
        }
    } else if (DisplayTBCount > bytesDouble) {
        double dGBValue = bytesDouble / GBCount;
        if (10 > dGBValue) {
            flowValueStr = QString::number(dGBValue, 'f', 2) + "G";
        } else if (100 > dGBValue) {
            flowValueStr = QString::number(dGBValue, 'f', 1) + "G";
        } else if (100 <= dGBValue) {
            flowValueStr = QString::number(dGBValue, 'f', 0) + "G";
        }
    } else if (DisplayTBCount >= bytesDouble) {
        // 大于TB单位
        double dTBValue = bytesDouble / TBCount;
        if (10 > dTBValue) {
            flowValueStr = QString::number(dTBValue, 'f', 2) + "T";
        } else if (100 > dTBValue) {
            flowValueStr = QString::number(dTBValue, 'f', 1) + "T";
        } else if (100 <= dTBValue) {
            flowValueStr = QString::number(dTBValue, 'f', 0) + "T";
        }
    }
    return flowValueStr;
}

QString getAppNameFromDesktop(const QString &desktopPath)
{
    QString appName;
    QString sysLanguage = QLocale::system().name();
    QString sysLanguagePrefix = sysLanguage.split("_").first();

    // 是否为界面应用
    QSettings readIniSettingMethod(desktopPath, QSettings::Format::IniFormat);
    QTextCodec *textCodec = QTextCodec::codecForName("UTF-8");
    readIniSettingMethod.setIniCodec(textCodec);
    readIniSettingMethod.beginGroup("Desktop Entry");
    // 应用名称
    QString xDeepinVendor = readIniSettingMethod.value("X-Deepin-Vendor").toString();
    if (X_DEEPIN_VENDOR_STR == xDeepinVendor) {
        appName = readIniSettingMethod.value(QString("GenericName[%1]").arg(sysLanguage)).toString();
        // 如果没获取到语言对应的应用名称，则获取语言前缀对应的应用名称
        if (appName.isEmpty()) {
            appName = readIniSettingMethod.value(QString("GenericName[%1]").arg(sysLanguagePrefix)).toString();
        }
    } else {
        appName = readIniSettingMethod.value(QString("Name[%1]").arg(sysLanguage)).toString();
        // 如果没获取到语言对应的应用名称，则获取语言前缀对应的应用名称
        if (appName.isEmpty()) {
            appName = readIniSettingMethod.value(QString("Name[%1]").arg(sysLanguagePrefix)).toString();
        }
    }

    if (appName.isEmpty()) {
        appName = readIniSettingMethod.value("Name").toString();
    }

    return appName;
}

QString getExeCmdFromDesktop(const QString &desktopPath)
{
    QSettings readIniSettingMethod(desktopPath, QSettings::Format::IniFormat);
    QTextCodec *textCodec = QTextCodec::codecForName("UTF-8");
    readIniSettingMethod.setIniCodec(textCodec);
    readIniSettingMethod.beginGroup("Desktop Entry");
    QString exeCmd = readIniSettingMethod.value("Exec").toString();

    return exeCmd;
}
