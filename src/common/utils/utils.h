#pragma once

#include <QString>

// 以byte为基准
const unsigned long KBCount = 1 << 10;
const unsigned long MBCount = 1 << 20;
const unsigned long GBCount = 1 << 30;
const unsigned long TBCount = long(1) << 40;

// 网速显示需要的进制（以1000为进制）
const unsigned long DisplayKBCount = 1000;
const unsigned long DisplayMBCount = DisplayKBCount << 10;
const unsigned long DisplayGBCount = DisplayMBCount << 10;
const unsigned long DisplayTBCount = DisplayGBCount << 10;

QString formatBytes(double input, int prec);
QString formatBytesByDefault(unsigned long input);
QString getAppNameFromDesktop(const QString &desktopPath);
QString getExeCmdFromDesktop(const QString &desktopPath);
