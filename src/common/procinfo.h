#pragma once

#include <QtCore/QString>
#include <QtDBus/QtDBus>
#include <QDBusMetaType>
#include <QtCore/QList>

struct ProcInfo {
    int nPid;
    QString sExecPath;          // 执行路径
    bool isbSysApp;             // 是否是系统应用
    QString sProcName;          //  进程名
    // 启动器应用信息
    QString sDesktopPath;       // 桌面文件路径
    QString sPkgName;           // 包名
    QString sAppName;           // 应用名
    QString sThemeIcon;         // 图标路径
    QString sID;                // 启动器中对应应用id
    qint64 nCategoryID;         // 启动器中分类id
    qint64 nTimeInstalled;      // 安装时间
    // 流量信息
    QString sTimeyMdh;          // 流量信息时间
    double dDownloadSpeed;      // 下载速度
    double dUploadSpeed;        // 上传速度
    double dDownloads;          // 下载量
    double dUploads;            // 上传量

    // 联网管控信息
    QString sStatus;            // 状态 询问/允许/禁止
    QString sDefaultStatus;     // 默认状态

    // 其他参数
    QString sOtherParam;
    bool operator!=(const ProcInfo &procInfo);
    // 初始化
    ProcInfo() {
        nPid = 0;
        isbSysApp = false;
        nCategoryID = 0;
        nTimeInstalled = 0;
        dDownloadSpeed = 0.0;
        dUploadSpeed = 0.0;
        dDownloads = 0.0;
        dUploads = 0.0;
    }
};

Q_DECLARE_METATYPE(ProcInfo);

QDBusArgument &operator<<(QDBusArgument &argument, const ProcInfo &procInfo);
const QDBusArgument &operator>>(const QDBusArgument &argument, ProcInfo &procInfo);

void registerProcInfoMetaType();
