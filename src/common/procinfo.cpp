#include "procinfo.h"

bool ProcInfo::operator!=(const ProcInfo &procInfo)
{
    return !(this->nPid == procInfo.nPid);
}

QDBusArgument &operator<<(QDBusArgument &argument, const ProcInfo &procInfo)
{
    argument.beginStructure();
    argument << procInfo.nPid << procInfo.sExecPath << procInfo.isbSysApp << procInfo.sProcName
             << procInfo.sDesktopPath << procInfo.sPkgName << procInfo.sAppName << procInfo.sThemeIcon
             << procInfo.sID << procInfo.nCategoryID << procInfo.nTimeInstalled
             << procInfo.sTimeyMdh << procInfo.dDownloadSpeed << procInfo.dUploadSpeed << procInfo.dDownloads << procInfo.dUploads
             << procInfo.sStatus << procInfo.sDefaultStatus << procInfo.sOtherParam;
    argument.endStructure();
    return argument;
}

const QDBusArgument &operator>>(const QDBusArgument &argument, ProcInfo &procInfo)
{
    argument.beginStructure();
    argument >> procInfo.nPid >> procInfo.sExecPath >> procInfo.isbSysApp >> procInfo.sProcName
             >> procInfo.sDesktopPath >> procInfo.sPkgName >> procInfo.sAppName >> procInfo.sThemeIcon
             >> procInfo.sID >> procInfo.nCategoryID >> procInfo.nTimeInstalled
             >> procInfo.sTimeyMdh >> procInfo.dDownloadSpeed >> procInfo.dUploadSpeed >> procInfo.dDownloads >> procInfo.dUploads
             >> procInfo.sStatus >> procInfo.sDefaultStatus >> procInfo.sOtherParam;
    argument.endStructure();
    return argument;
}

void registerProcInfoMetaType()
{
    qRegisterMetaType<ProcInfo>("ProcInfo");
    qDBusRegisterMetaType<ProcInfo>();
}
