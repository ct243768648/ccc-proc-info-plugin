#include "procinfolist.h"

void registerProcInfoListMetaType()
{
    qRegisterMetaType<ProcInfoList>("ProcInfoList");
    qDBusRegisterMetaType<ProcInfoList>();
}
