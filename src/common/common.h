#pragma once

#include <QDBusArgument>
#include <QtCore>


#define APP_PKG_NAME "com.github.ccc-proc-info-plugin"

const QString CccSysDaemonPkgName = "com.github.ccc-sys-daemon";
const QString CccSysDaemonPkgVer = "0.0.2";
const QString CccSysDaemonPkgArch = "amd64";
const QString CccSysDaemonPkgFilePath = QString("/opt/apps/%1/files/pkgs/%2_%3_%4.deb")
        .arg(APP_PKG_NAME).arg(CccSysDaemonPkgName).arg(CccSysDaemonPkgVer).arg(CccSysDaemonPkgArch);

#define CCC_SYS_DAEMON_DBUS_NAME "com.ccc.daemon"
#define CCC_SYS_DAEMON_DBUS_PATH "/com/ccc/daemon"
#define CCC_SYS_DAEMON_DBUS_IFC "com.ccc.daemon"

#define LOGIN1_DBUS_NAME "org.freedesktop.login1"
#define LOGIN1_DBUS_PATH "/org/freedesktop/login1"
#define LOGIN1_MANAGER_DBUS_IFC "org.freedesktop.login1.Manager"
#define LOGIN1_SESSION_DBUS_IFC "org.freedesktop.login1.Session"
#define PROPS_DBUS_IFC "org.freedesktop.DBus.Properties"

#define PROC_INFO_DBUS_NAME "com.ccc.daemon.ProcInfo"
#define PROC_INFO_DBUS_PATH "/com/ccc/daemon/ProcInfo"
#define PROC_INFO_DBUS_IFC "com.ccc.daemon.ProcInfo"

// 配置文件路径
#define CFG_RELATIVE_DIR_PATH APP_PKG_NAME
#define DEFAULT_CFG_FILE_NAME "default.cfg"
#define CFG_FILE_NAME "all.cfg"
// 默认配置目录
const QString DefaultCfgFilePath = QString("/opt/apps/%1/files/%2")
        .arg(CFG_RELATIVE_DIR_PATH).arg(DEFAULT_CFG_FILE_NAME);
const QString CfgFilePath = QString("%1/%2/%3").arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation))
        .arg(CFG_RELATIVE_DIR_PATH).arg(CFG_FILE_NAME);

#define CFG_KEY_NET "network"
#define CFG_KEY_NET_DATA_MONITOR_SWITCH "net_data_monitor_switch"
#define CFG_KEY_NET_SHOW_NET_UNKNOWN_ITEM_SWITCH "show_net_unknown_item"
// 系统单元
#define CFG_KEY_SYSTEM "system"
// 内存清理应用白名单
#define CFG_KEY_MEM_CLEAN_APP_WHITELIST "memory_clean_app_whitelist"

// 执行参数配置文件
#define EXEC_PARAMS_CFG_FILE_NAME "exec_params.cfg"
const QString ExecParamsCfgFilePath = QString("%1/%2/%3").arg(QStandardPaths::writableLocation(QStandardPaths::ConfigLocation))
        .arg(CFG_RELATIVE_DIR_PATH).arg(EXEC_PARAMS_CFG_FILE_NAME);

#define EXEC_PARAMS_CFG_KEY_OPEN_SETTINGS_PAGE "open_settings_page"
#define EXEC_PARAMS_CFG_KEY_OPEN_ABOUT_DLG "open_about_dlg"

//// proc cleaner
// 禁止强杀的应用包名列表
const QStringList ForbiddenForceKillAppPkgNameList = {APP_PKG_NAME};
// 需要隐藏显示的应用包名列表
const QStringList NeedHideAppPkgNameList = {APP_PKG_NAME};

// 图标
#define THEME_ICON_NAME_DEFAULT "application-x-executable"

// org.freedesktop.login dbus 会话信息结构体
struct LoginSessionInfo {
    QString id;
    unsigned int uid;
    QString name;
    QString seat;
    QDBusObjectPath objPath;
};
Q_DECLARE_METATYPE(LoginSessionInfo);
Q_DECLARE_METATYPE(QList<LoginSessionInfo>)
// 注册会话信息dbus结构体重载操作符函数
QDBusArgument &operator<<(QDBusArgument &argument, const LoginSessionInfo &info);
const QDBusArgument &operator>>(const QDBusArgument &argument, LoginSessionInfo &info);

// 应用信息结构体
struct AppInfo {
    QString pkgName;
    QString name;
    QString icon;
    QString desktopFilePath;
    ulong memorySize;
    bool isLocked;
    QList<uint> pids;
    void info() {
        icon = THEME_ICON_NAME_DEFAULT;
        isLocked = false;
    }
};

// 应用显示项放下动作
enum AppViewItemDroppedAction {
    Restore, // 还原
    Remove, // 移除
    Lock // 锁定
};
