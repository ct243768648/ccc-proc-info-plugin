#pragma once

#include "common/common.h"
#include "common/procinfolist.h"
#include "common/utils/utils.h"

#include <dtkwidget_global.h>

#include <DPalette>
#include <DLabel>

#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QPainter>
#include <QPainterPath>
#include <QWidget>

class QDBusInterface;

using namespace Dtk::Widget;
DWIDGET_USE_NAMESPACE

class ActionWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ActionWidget(const QIcon &icon, const QString &text, QWidget *parent = nullptr)
    : QWidget(parent)
    , m_focusIn(false)
    , m_textLabel(nullptr) {
        QHBoxLayout *mainLayout = new QHBoxLayout;
        mainLayout->setContentsMargins(7, 6, 9, 6);
        mainLayout->setSpacing(2);
        setLayout(mainLayout);

        QLabel *iconLable = new QLabel(this);
        iconLable->setFixedHeight(20);
        iconLable->setFixedWidth(20);
        iconLable->setPixmap(icon.pixmap(20, 20));
        mainLayout->addWidget(iconLable, 0, Qt::AlignLeft);

        m_textLabel = new QLabel(this);
        m_textLabel->setFixedHeight(20);
        m_textLabel->setText(text);
        mainLayout->addWidget(m_textLabel, 1, Qt::AlignLeft);
    }

    ~ActionWidget()
    {
    }

protected:
    void paintEvent(QPaintEvent *event)
    {
        QRectF rectangle(0, 0, width(), height());
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        DPalette pa;
        QColor color = pa.color(DPalette::Highlight);
        if (m_focusIn) { // hover效果
            QPainterPath pp;
            pp.addRect(rectangle);
            painter.fillPath(pp, color);
        }
        QWidget::paintEvent(event);
        update();
    }

    void enterEvent(QEvent *event)
    {
        m_focusIn = true;
        m_textLabel->setForegroundRole(DPalette::Base);
        QWidget::enterEvent(event);
        update();
    }

    void leaveEvent(QEvent *event)
    {
        m_focusIn = false;
        m_textLabel->setForegroundRole(DPalette::ToolTipText);
        QWidget::leaveEvent(event);
        update();
    }

private:
    bool m_focusIn;
    QLabel *m_textLabel;
};

class ProcInfoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ProcInfoWidget(QWidget *parent = nullptr);

    QSize sizeHint() const;

private:
    bool loadCfgsFromFilePath(const QString &path);
    void loadCfgs();
    void enableNetDataMonitor(bool enable);
    void getNetBytes(unsigned long &upload, unsigned long &download);

protected:
    //    void resizeEvent(QResizeEvent *event);
    //    void mousePressEvent(QMouseEvent *event);

Q_SIGNALS:
    void sendSpeed(unsigned long upload, unsigned long download);

public Q_SLOTS:
    void updateProcInfo();
    void updateNetSpeed();
    void onLogin1SelfSessionPropsChanged(const QString &ifcName, const QVariantMap &propMap, const QStringList &args);

private:
    QDBusInterface *m_netMonitorInter;
    bool m_enableNetDataMonitor;
    ProcInfoList m_procInfoList;
    DLabel *m_speedLabel;
};
