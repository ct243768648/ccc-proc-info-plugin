#pragma once

#include "ProcInfoSessionDaemonInter.h"

#include <QObject>

const QString service = "com.ccc.daemon.ProcInfo";
const QString path = "/com/ccc/daemon/ProcInfo";

using ProcInfoSessionDaemonInter = com::ccc::daemon::ProcInfo;

// 数据处理类
class ProcInfoModel : public QObject
{
    Q_OBJECT
private:
    void initData();

public:
    explicit ProcInfoModel(QObject *parent = nullptr);
    ~ProcInfoModel();

    void AsyncExecProgram(const QString &program, const QStringList &arguments);

public Q_SLOTS:

Q_SIGNALS:

private:
    ProcInfoSessionDaemonInter *m_procInfoSessionDaemonInter;
};
