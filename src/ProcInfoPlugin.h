#ifndef SYSINFOPLUGIN_H
#define SYSINFOPLUGIN_H

#include "procinfowidget.h"
#include "ProcInfoModel.h"

#include <QObject>
#include <DLabel>

#include <pluginsiteminterface.h>

DWIDGET_USE_NAMESPACE

class ProcInfoPlugin : public QObject
    , PluginsItemInterface
{
    Q_OBJECT
    // 声明实现了的接口
    Q_INTERFACES(PluginsItemInterface)
    // 插件元数据
    Q_PLUGIN_METADATA(IID ModuleInterface_iid FILE "procinfo.json")

public:
    explicit ProcInfoPlugin(QObject *parent = nullptr);

    // 返回插件的名称，必须是唯一值，不可以和其它插件冲突
    const QString pluginName() const override;

    const QString pluginDisplayName() const override;

    // 插件初始化函数
    void init(PluginProxyInterface *proxyInter) override;

    // 返回插件的 widget
    QWidget *itemWidget(const QString &itemKey) override;

    QWidget *itemTipsWidget(const QString &itemKey) override;

    bool pluginIsAllowDisable() override;
    bool pluginIsDisable() override;
    void pluginStateSwitched() override;

    int itemSortKey(const QString &itemKey) override;
    void setSortKey(const QString &itemKey, const int order) override;

    const QString itemCommand(const QString &itemKey) override;
    const QString itemContextMenu(const QString &itemKey) override;
    void invokedMenuItem(const QString &itemKey, const QString &menuId, const bool checked) override;
    void displayModeChanged(const Dock::DisplayMode displayMode) override;
    void positionChanged(const Dock::Position position) override;

    void pluginSettingsChanged() override;

    PluginSizePolicy pluginSizePolicy() const override;

private:
    ProcInfoModel *m_model;
    ProcInfoWidget *m_procInfoWidget;
    DLabel *m_tipLabel;
};

#endif // SYSINFOPLUGIN_H
