#include "ProcInfoModel.h"

void ProcInfoModel::initData()
{
    m_procInfoSessionDaemonInter = new ProcInfoSessionDaemonInter(service, path,
                                                                  QDBusConnection::sessionBus(),
                                                                  this);
}

ProcInfoModel::ProcInfoModel(QObject *parent)
    : QObject(parent)
    , m_procInfoSessionDaemonInter(nullptr)
{
    initData();
}

ProcInfoModel::~ProcInfoModel()
{
}

void ProcInfoModel::AsyncExecProgram(const QString &program, const QStringList &arguments)
{
    m_procInfoSessionDaemonInter->AsyncExecProgram(program, arguments);
}
